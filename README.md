# Relapositional classidless **CSS-method**

simple, flexible & modular way to structure styles relying on native html-elements, inheriting values, variables and document tree relations.

## Install

1) **get the files**: `git submodule add https://codeberg.org/ideatmo/cs.git` or `git clone https://codeberg.org/ideatmo/cs.git`

2) **include stylesheet**: + `import '/src/cs/A.less';` or + `<link href="./cs/A.css" type="text/css" rel="stylesheet">`